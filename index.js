var app = Elm.Main.init({
  node: document.getElementById('elm')
});

app.ports.makeLayout.subscribe(function(args) {
  var graphId, nodes, links;
  [graphId, nodes, links] = args;
  var simulation = d3.forceSimulation()
    .stop()
    .force("link", d3.forceLink().id(function(d) { return d.nodeId; }))
    .force("charge", d3.forceManyBody())
    .force("center", d3.forceCenter(50, 50));

  simulation.nodes(nodes);
  simulation.force("link").links(links);
  simulation.tick(50);

  app.ports.receiveLayout.send([graphId, simulation.nodes()]);
});
