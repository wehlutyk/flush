module Colors exposing
    ( baseNodeColor
    , baseNodeColorHex
    , blackColor
    , buttonBgColor
    , edgeColor
    , edgeColorHex
    , flipNodeColor
    , flipNodeColorHex
    , greyColor
    , lightGreyColor
    , seedNodeColor
    , seedNodeColorHex
    , selectedStrokeColor
    , selectedStrokeColorHex
    , transparentColor
    , whiteColor
    )

import Element exposing (rgb255, rgba)


baseNodeColorHex =
    "#a8bdad"


baseNodeColor =
    rgb255 168 189 173


selectedStrokeColorHex =
    "#d6001a"


selectedStrokeColor =
    rgb255 214 0 26


seedNodeColorHex =
    "#add8e6"


seedNodeColor =
    rgb255 173 216 230


flipNodeColorHex =
    "#e79050"


flipNodeColor =
    rgb255 231 144 80


edgeColorHex =
    "#d3d3d3"


edgeColor =
    rgb255 211 211 211


transparentColor =
    rgba 0.0 0.0 0.0 0.0


whiteColor =
    rgb255 255 255 255


blackColor =
    rgb255 0 0 0


greyColor =
    rgb255 128 128 128


lightGreyColor =
    rgb255 200 200 200


buttonBgColor =
    rgb255 227 227 255
