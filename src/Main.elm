port module Main exposing (main)

import Browser
import Dict
import Element
import Graph
import IntDict
import List
import List.Extra exposing (cartesianProduct, dropWhile, uniquePairs, zip)
import List.Selection as Selection
import Random
import Random.List exposing (shuffle)
import Selection exposing (dropAboveSelection, pushAndSelect, selectNextOrInit, selectPreviousOrNone)
import Set
import Time
import Types exposing (..)
import View


main =
    Browser.element
        { init = init
        , update = update
        , view = Element.layout [] << View.view
        , subscriptions = subscriptions
        }



-- INIT


init : () -> ( Model, Cmd Msg )
init _ =
    ( initialModel
    , initialModel.savedStories
        |> Selection.toList
        |> List.map (Tuple.second >> .graph)
        |> (::) initialModel.story.graph
        |> List.map layoutParams
        |> List.map makeLayout
        |> Cmd.batch
    )



-- PORTS


port makeLayout : ( String, List NodeAsId, List LinkAsIds ) -> Cmd msg


port receiveLayout : (( String, List { nodeId : Int, x : Float, y : Float } ) -> msg) -> Sub msg



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    let
        ticks =
            case model.playing of
                True ->
                    Time.every 100 (\_ -> StepForward)

                False ->
                    Sub.none

        layout =
            receiveLayout NewGraphLayout
    in
    Sub.batch [ ticks, layout ]



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        StepForward ->
            let
                story =
                    model.story

                ( newStory, newPlaying ) =
                    case isAtEnd model.story.history of
                        False ->
                            ( { story | history = updateTransitions selectNextOrInit story.history }, model.playing )

                        True ->
                            let
                                availableNodeIds =
                                    untouchedNodeIds model.story

                                gAvailableNodeIds =
                                    availableNodeIds
                                        |> Set.toList
                                        |> shuffle

                                ( shuffledAvailableNodeIds, newSeed ) =
                                    seed story.history
                                        |> Random.step gAvailableNodeIds

                                nodeFlips nodeId =
                                    case Graph.get nodeId story.graph of
                                        Nothing ->
                                            False

                                        Just nodeContext ->
                                            let
                                                threshold =
                                                    nodeContext.node.label

                                                incomingNodeIds =
                                                    nodeContext
                                                        |> .incoming
                                                        |> IntDict.keys
                                                        |> Set.fromList

                                                activeIncomingNodeIds =
                                                    Set.diff incomingNodeIds availableNodeIds
                                            in
                                            if Set.size incomingNodeIds == 0 then
                                                False

                                            else
                                                toFloat (Set.size activeIncomingNodeIds) / toFloat (Set.size incomingNodeIds) >= threshold

                                flipSearch =
                                    shuffledAvailableNodeIds
                                        |> dropWhile (not << nodeFlips)
                            in
                            case flipSearch of
                                [] ->
                                    ( story, False )

                                id :: _ ->
                                    ( { story
                                        | history = pushAndSelectWithSeed newSeed (Flip id) story.history
                                      }
                                    , model.playing
                                    )
            in
            ( { model
                | story = newStory
                , savedStories = Selection.deselect model.savedStories
                , playing = newPlaying
              }
            , Cmd.none
            )

        StepBackward ->
            let
                story =
                    model.story

                newStory =
                    { story | history = updateTransitions selectPreviousOrNone story.history }
            in
            ( { model
                | story = newStory
                , savedStories = Selection.deselect model.savedStories
                , playing = False
              }
            , Cmd.none
            )

        StepTo transition ->
            let
                story =
                    model.story

                newStory =
                    { story | history = updateTransitions (Selection.select transition) story.history }
            in
            ( { model
                | story = newStory
                , savedStories = Selection.deselect model.savedStories
                , playing = False
              }
            , Cmd.none
            )

        Reset ->
            let
                story =
                    model.story

                newStory =
                    { story | history = updateTransitions dropAboveSelection story.history }
            in
            ( { model
                | story = newStory
                , savedStories = Selection.deselect model.savedStories
              }
            , Cmd.none
            )

        SeedNode nodeId ->
            let
                story =
                    model.story

                newStory =
                    { story
                        | history = updateTransitions (pushAndSelect <| Seed nodeId) story.history
                    }
            in
            ( { model
                | story = newStory
                , savedStories = Selection.deselect model.savedStories
              }
            , Cmd.none
            )

        HistorySeed seedString ->
            case ( isEmpty model.story.history, String.toInt seedString ) of
                ( False, _ ) ->
                    ( model, Cmd.none )

                ( _, Nothing ) ->
                    ( model, Cmd.none )

                ( True, Just seedInt ) ->
                    let
                        story =
                            model.story

                        newStory =
                            { story | history = fromSeedInt seedInt }
                    in
                    ( { model
                        | story = newStory
                        , savedStories = Selection.deselect model.savedStories
                      }
                    , Cmd.none
                    )

        Save ->
            ( { model
                | savedStories = pushAndSelect ( model.generator, model.story ) model.savedStories
              }
            , Cmd.none
            )

        Load ( generator, story ) ->
            ( { model
                | story = story
                , generator = generator
                , savedStories = Selection.select ( generator, story ) model.savedStories
              }
            , Cmd.none
            )

        Play ->
            ( { model | playing = True }, Cmd.none )

        Pause ->
            ( { model | playing = False }, Cmd.none )

        Generate ->
            let
                graph =
                    generateGraph model.generator
            in
            ( { model
                | story = fromGraph graph
                , playing = False
              }
            , makeLayout <| layoutParams graph
            )

        NewGraphLayout ( structureId, layout ) ->
            let
                graphLayout =
                    layout
                        |> List.map (\{ nodeId, x, y } -> ( nodeId, Position x y ))
                        |> Dict.fromList

                layouts =
                    Dict.insert structureId (Ready graphLayout) model.layouts
            in
            ( { model | layouts = layouts }
            , Cmd.none
            )

        NetworkGeneratorSetN n ->
            let
                networkGenerator =
                    Tuple.first model.generator

                newNetworkGenerator =
                    { networkGenerator | n = n }
            in
            ( { model
                | generator = ( newNetworkGenerator, Tuple.second model.generator )
              }
            , Cmd.none
            )

        NetworkGeneratorSetP p ->
            let
                networkGenerator =
                    Tuple.first model.generator

                newNetworkGenerator =
                    { networkGenerator | p = p }
            in
            ( { model
                | generator = ( newNetworkGenerator, Tuple.second model.generator )
              }
            , Cmd.none
            )

        NetworkGeneratorSetSeedInt seedString ->
            case String.toInt seedString of
                Just seed ->
                    let
                        networkGenerator =
                            Tuple.first model.generator

                        newNetworkGenerator =
                            { networkGenerator | seed = seed }
                    in
                    ( { model
                        | generator = ( newNetworkGenerator, Tuple.second model.generator )
                      }
                    , Cmd.none
                    )

                Nothing ->
                    ( model, Cmd.none )

        PickThresholdsGenerator thresholdsGeneratorType ->
            let
                thresholdsGenerator =
                    case thresholdsGeneratorType of
                        DiracType ->
                            initialDirac

                        NormalType ->
                            initialNormal
            in
            ( { model
                | generator = ( Tuple.first model.generator, thresholdsGenerator )
              }
            , Cmd.none
            )

        ThresholdsGeneratorSetLocation loc ->
            let
                thresholdsGenerator =
                    case Tuple.second model.generator of
                        Dirac _ ->
                            Dirac loc

                        Normal seed _ sigma ->
                            Normal seed loc sigma
            in
            ( { model
                | generator = ( Tuple.first model.generator, thresholdsGenerator )
              }
            , Cmd.none
            )

        ThresholdsGeneratorSetStd std ->
            let
                thresholdsGenerator =
                    case Tuple.second model.generator of
                        Dirac _ ->
                            Tuple.second model.generator

                        Normal seed mu _ ->
                            Normal seed mu std
            in
            ( { model
                | generator = ( Tuple.first model.generator, thresholdsGenerator )
              }
            , Cmd.none
            )

        ThresholdsGeneratorSetSeedInt seedString ->
            case String.toInt seedString of
                Just newSeed ->
                    let
                        thresholdsGenerator =
                            case Tuple.second model.generator of
                                Dirac _ ->
                                    Tuple.second model.generator

                                Normal _ mu sigma ->
                                    Normal newSeed mu sigma
                    in
                    ( { model
                        | generator = ( Tuple.first model.generator, thresholdsGenerator )
                      }
                    , Cmd.none
                    )

                Nothing ->
                    ( model, Cmd.none )
