module View exposing (view)

import Colors exposing (..)
import Dict exposing (Dict)
import Element
    exposing
        ( Element
        , alignLeft
        , alignRight
        , behindContent
        , centerX
        , centerY
        , column
        , el
        , explain
        , fill
        , fillPortion
        , height
        , html
        , maximum
        , minimum
        , none
        , padding
        , px
        , row
        , scrollbarX
        , spacing
        , text
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import List.Selection as Selection
import Types exposing (..)
import View.Graph


roundTo : Float -> Float -> Float
roundTo precision number =
    toFloat (round (number * precision)) / precision


view { generator, layouts, story, playing, savedStories } =
    column
        [ width <| maximum 1000 fill
        , centerX
        , centerY
        , spacing 20
        ]
        [ savedStoriesView layouts savedStories
        , row
            [ width fill
            , height <| fillPortion 8

            --, Border.widthEach { top = 1, bottom = 0, right = 0, left = 0 }
            , Border.color greyColor
            ]
            [ generatorView generator
            , graphView True 1 layouts story
            ]
        , row
            [ width fill
            , height <| maximum 60 fill
            , spacing 10
            , padding 5
            ]
            [ Input.text
                [ width (px 50)
                , height (px 30)
                ]
                { onChange = HistorySeed
                , text = seedInt story.history |> String.fromInt
                , placeholder = Nothing
                , label = Input.labelLeft [ centerY, padding 5 ] (text "Seed")
                }
            , button (Just Reset) (text "↺")
            , button (Just StepBackward) (text "⧏")
            , if playing then
                button (Just Pause) (text "⏯")

              else
                button (Just Play) (text "▶")
            , el [ alignLeft ] <|
                button (Just StepForward) (text "⧐")
            , stepsView story.history
            , el [ alignRight ] <|
                button (Just Save) (text "save")
            ]
        ]


button onPress label =
    Input.button
        [ padding 10
        , Border.rounded 4
        , Background.color buttonBgColor
        ]
        { onPress = onPress, label = el [ centerX, centerY ] label }


generatorView generator =
    column
        [ width (minimum 200 fill)
        , height fill
        , padding 10
        , spacing 20
        ]
        [ el [ centerX ] (button (Just Generate) (text "Generate"))
        , el
            [ Border.widthEach { top = 0, bottom = 1, right = 0, left = 0 }
            , Border.color lightGreyColor
            , padding 10
            , width fill
            ]
          <|
            el [ centerX ] (text "Structure")
        , networkGeneratorView <| Tuple.first generator
        , el
            [ Border.widthEach { top = 0, bottom = 1, right = 0, left = 0 }
            , Border.color lightGreyColor
            , padding 10
            , width fill
            ]
          <|
            el [ centerX ] (text "Thresholds")
        , thresholdsGeneratorView <| Tuple.second generator
        ]


networkGeneratorView { n, p, seed } =
    column [ width fill, padding 10, spacing 20 ]
        [ sliderOption (round >> NetworkGeneratorSetN)
            ("n = " ++ String.fromInt n)
            ( 0, 1000 )
            (Just 1)
            (toFloat n)
        , sliderOption NetworkGeneratorSetP
            ("p = " ++ String.fromFloat p ++ "\n<z> = " ++ String.fromFloat (roundTo 100 (p * toFloat n)))
            ( 0, 0.2 )
            (Just 0.001)
            p
        , Input.text
            [ width (px 50)
            , height (px 30)
            ]
            { onChange = NetworkGeneratorSetSeedInt
            , text = seed |> String.fromInt
            , placeholder = Nothing
            , label = Input.labelLeft [ centerY, padding 5 ] (text "Seed")
            }
        ]


sliderOption onChange labelText ( min, max ) step value =
    Input.slider
        [ height (px 30)
        , behindContent
            (el
                [ width fill
                , height (px 2)
                , centerY
                , Background.color greyColor
                , Border.rounded 2
                ]
                none
            )
        ]
        { onChange = onChange
        , label =
            Input.labelAbove [ alignLeft ] (text labelText)
        , min = min
        , max = max
        , step = step
        , value = value
        , thumb =
            Input.defaultThumb
        }


thresholdsGeneratorView thresholdsGenerator =
    let
        paramsView =
            case thresholdsGenerator of
                Dirac loc ->
                    [ sliderOption
                        ThresholdsGeneratorSetLocation
                        ("loc = " ++ String.fromFloat loc)
                        ( 0, 1 )
                        (Just 0.01)
                        loc
                    ]

                Normal seed mu sigma ->
                    [ sliderOption
                        ThresholdsGeneratorSetLocation
                        ("μ = " ++ String.fromFloat mu)
                        ( 0, 1 )
                        (Just 0.01)
                        mu
                    , sliderOption
                        ThresholdsGeneratorSetStd
                        ("σ = " ++ String.fromFloat sigma)
                        ( 0, 0.3 )
                        (Just 0.01)
                        sigma
                    , Input.text
                        [ width (px 50)
                        , height (px 30)
                        ]
                        { onChange = ThresholdsGeneratorSetSeedInt
                        , text = seed |> String.fromInt
                        , placeholder = Nothing
                        , label = Input.labelLeft [ centerY, padding 5 ] (text "Seed")
                        }
                    ]

        selectedOption =
            case thresholdsGenerator of
                Dirac _ ->
                    DiracType

                Normal _ _ _ ->
                    NormalType
    in
    column
        [ spacing 20
        , width fill
        ]
        [ el [ centerX ]
            (Input.radioRow [ spacing 5 ]
                { onChange = PickThresholdsGenerator
                , selected = Just <| selectedOption
                , options =
                    [ optionWith DiracType (text "Dirac")
                    , optionWith NormalType (text "Normal")
                    ]
                , label = Input.labelAbove [] none
                }
            )
        , column
            [ spacing 20
            , padding 10
            , width fill
            ]
            paramsView
        ]


optionWith onSelected label =
    let
        selectedColor s =
            if s == Input.Selected then
                lightGreyColor

            else
                transparentColor

        deselectedColor s =
            if s /= Input.Selected then
                greyColor

            else
                blackColor
    in
    Input.optionWith onSelected
        (\selected ->
            el
                [ padding 5
                , Border.width 1
                , Border.rounded 5
                , Border.color <| selectedColor selected
                , Font.color <| deselectedColor selected
                ]
                label
        )


savedStoriesView layouts genStories =
    row
        [ width fill
        , height <| maximum 80 fill
        ]
        (Selection.toList genStories
            |> List.reverse
            |> List.map (savedStoryView layouts <| Selection.selected genStories)
        )


savedStoryView : Dict String (Computable GraphLayout) -> Maybe ( Generator, Story ) -> ( Generator, Story ) -> Element Msg
savedStoryView layouts selected genStory =
    Input.button
        [ width <| maximum 80 fill
        , height fill
        , Border.width 1
        , Border.rounded 5
        , Border.color
            (if selected == Just genStory then
                lightGreyColor

             else
                transparentColor
            )
        , padding 3
        ]
        { onPress = Just <| Load genStory
        , label = html <| View.Graph.view False 1 layouts (Tuple.second genStory)
        }


graphView active scale layouts story =
    el
        [ width <| fillPortion 7
        , height fill
        , padding 20
        ]
        (html <| View.Graph.view active scale layouts story)


stepsView history =
    row
        [ width fill
        , height fill
        , scrollbarX
        ]
        (transitions history
            |> Selection.mapSelected
                { selected = \a -> ( True, a )
                , rest = \a -> ( False, a )
                }
            |> Selection.toList
            |> List.reverse
            |> List.map stepView
        )


stepView ( selected, transition ) =
    let
        ( background, nodeId ) =
            case transition of
                Flip id ->
                    ( flipNodeColor, id )

                Seed id ->
                    ( seedNodeColor, id )

        border =
            if selected then
                selectedStrokeColor

            else
                whiteColor

        label =
            el
                [ centerX, centerY, Font.color whiteColor ]
                (text <| String.fromInt nodeId)
    in
    Input.button
        [ Background.color background
        , Border.width 5
        , Border.color border
        , Border.rounded 50
        , height (px 40)
        , width (px 40)
        ]
        { onPress = Just <| StepTo transition
        , label = label
        }
