module View.Graph exposing (view)

import Colors exposing (..)
import Dict exposing (Dict)
import Graph
import List.Extra exposing (dropWhile)
import List.Selection as Selection
import Selection exposing (sAndBelow)
import Svg exposing (..)
import Svg.Attributes as Attrs exposing (..)
import Svg.Events exposing (..)
import Types exposing (..)


xmin =
    0


xmax =
    100


ymin =
    0


ymax =
    100


nodeRadius =
    5


nodeStrokeWidth =
    1


edgeWidth =
    1


view : Bool -> Float -> Dict String (Computable GraphLayout) -> Story -> Svg Msg
view active outerScale layouts story =
    let
        scale =
            outerScale * 2 / toFloat (Graph.size story.graph) ^ 0.25

        inner =
            case Dict.get (graphStructureId story.graph) layouts of
                Nothing ->
                    [ text_
                        [ x "50"
                        , y "50"
                        , textAnchor "middle"
                        , dominantBaseline "central"
                        , fontSize "0.25em"
                        , fill "gray"
                        ]
                        [ text "Missing layout" ]
                    ]

                Just Computing ->
                    [ text_
                        [ x "50"
                        , y "50"
                        , textAnchor "middle"
                        , dominantBaseline "central"
                        , fontSize "0.25em"
                        , fill "gray"
                        ]
                        [ text "Computing layout" ]
                    ]

                Just (Ready layout) ->
                    graphView active scale layout story
    in
    svg
        [ width "100%"
        , height "100%"
        , [ xmin, ymin, xmax, ymax ]
            |> List.map String.fromInt
            |> String.join " "
            |> viewBox
        ]
        inner


graphView : Bool -> Float -> GraphLayout -> Story -> List (Svg Msg)
graphView active scale layout story =
    let
        colors =
            nodeColors story.history

        minMax l =
            ( List.minimum l, List.maximum l )

        fit =
            let
                ( minX, maxX ) =
                    Dict.toList layout
                        |> List.map Tuple.second
                        |> List.map .x
                        |> minMax

                spanX =
                    Maybe.map2 (-) maxX minX

                ( minY, maxY ) =
                    Dict.toList layout
                        |> List.map Tuple.second
                        |> List.map .y
                        |> minMax

                spanY =
                    Maybe.map2 (-) maxY minY

                assemble minX_ spanX_ minY_ spanY_ =
                    { minX = minX_, spanX = spanX_, minY = minY_, spanY = spanY_ }

                sizes =
                    Maybe.map4 assemble minX spanX minY spanY

                totalNodeSize =
                    nodeRadius + nodeStrokeWidth
            in
            case sizes of
                Just sizes_ ->
                    \{ x, y } ->
                        { x = totalNodeSize * scale + (100 - 2 * totalNodeSize * scale) * (x - sizes_.minX) / sizes_.spanX
                        , y = totalNodeSize * scale + (100 - 2 * totalNodeSize * scale) * (y - sizes_.minY) / sizes_.spanY
                        }

                _ ->
                    \pos -> pos
    in
    [ g [] (Graph.edges story.graph |> List.map (edgeView scale fit layout story.graph))
    , g [] (Graph.nodes story.graph |> List.map (nodeView active scale fit layout story.history colors))
    ]


nodeView active scale fit layout history ( nodeFills, nodeStrokes ) { id, label } =
    let
        threshold =
            label

        rawPos =
            Dict.get id layout
                |> Maybe.withDefault (Position 0.0 0.0)

        pos =
            fit rawPos

        clickAttrs =
            case ( active, isAtEnd history, Dict.member id nodeFills ) of
                ( True, True, False ) ->
                    [ onClick <| SeedNode id
                    , Attrs.cursor "pointer"
                    ]

                ( _, _, _ ) ->
                    [ Attrs.cursor "default" ]
    in
    g clickAttrs
        [ circle
            [ cx <| String.fromFloat pos.x
            , cy <| String.fromFloat pos.y
            , r <| String.fromFloat (nodeRadius * scale)
            , fill
                (nodeFills
                    |> Dict.get id
                    |> Maybe.withDefault baseNodeColorHex
                )
            , stroke
                (nodeStrokes
                    |> Dict.get id
                    |> Maybe.withDefault "transparent"
                )
            , strokeWidth <| String.fromInt nodeStrokeWidth
            ]
            []
        , text_
            [ x <| String.fromFloat pos.x
            , y <| String.fromFloat (pos.y - nodeRadius * scale)
            , textAnchor "middle"
            , dominantBaseline "hanging"
            , fontSize <| String.fromFloat (nodeRadius * scale / 40) ++ "em"
            , fill "white"
            ]
            [ text <| String.fromInt id ]
        , text_
            [ x <| String.fromFloat pos.x
            , y <| String.fromFloat pos.y
            , textAnchor "middle"
            , dominantBaseline "central"
            , fontSize <| String.fromFloat (nodeRadius * scale / 25) ++ "em"
            , fill "white"
            ]
            [ text <| String.fromFloat <| (toFloat <| round <| 100 * threshold) / 100 ]
        ]


edgeView scale fit layout graph { from, to } =
    let
        getPos nodeId g =
            Dict.get nodeId layout
                |> Maybe.map fit
    in
    case ( getPos from graph, getPos to graph ) of
        ( Just fromPos, Just toPos ) ->
            line
                [ x1 <| String.fromFloat fromPos.x
                , y1 <| String.fromFloat fromPos.y
                , x2 <| String.fromFloat toPos.x
                , y2 <| String.fromFloat toPos.y
                , stroke edgeColorHex
                , strokeWidth <| String.fromFloat (edgeWidth * scale)
                ]
                []

        ( _, _ ) ->
            -- This should never happen
            g [] []


nodeColors history =
    let
        nowAndPast =
            transitions history
                |> sAndBelow

        nodeStrokesTuples =
            case nowAndPast of
                [] ->
                    []

                (Flip id) :: _ ->
                    [ ( id, selectedStrokeColorHex ) ]

                (Seed id) :: _ ->
                    [ ( id, selectedStrokeColorHex ) ]
    in
    ( nowAndPast
        |> List.map transitionNodeColorTuple
        |> Dict.fromList
    , nodeStrokesTuples
        |> Dict.fromList
    )


transitionNodeColorTuple transition =
    case transition of
        Flip id ->
            ( id, flipNodeColorHex )

        Seed id ->
            ( id, seedNodeColorHex )
