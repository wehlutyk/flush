module Types exposing
    ( Computable(..)
    , Generator
    , GraphLayout
    , History
    , LinkAsIds
    , Model
    , Msg(..)
    , NetworkGenerator
    , NodeAsId
    , Position
    , Story
    , ThresholdsGenerator(..)
    , ThresholdsGeneratorType(..)
    , Transition(..)
    , fromGraph
    , fromSeedInt
    , generateGraph
    , graphStructureId
    , initialDirac
    , initialHistorySeedInt
    , initialModel
    , initialNormal
    , isAtEnd
    , isEmpty
    , layoutParams
    , pushAndSelectWithSeed
    , seed
    , seedInt
    , transitions
    , untouchedNodeIds
    , updateTransitions
    )

import Dict exposing (Dict)
import Graph exposing (Graph, NodeId, nodeIds)
import List.Extra exposing (cartesianProduct, dropWhile, last, takeWhile, uniquePairs, zip)
import List.Selection as Selection exposing (Selection)
import Random
import Selection exposing (aboveS, pushAndSelect)
import Set


graphStructureId : Graph a b -> String
graphStructureId graph =
    let
        nodesString =
            Graph.nodeIds graph
                |> List.map String.fromInt
                |> String.join ","

        edgesString =
            Graph.edges graph
                |> List.map (\{ from, to } -> String.fromInt from ++ ">" ++ String.fromInt to)
                |> String.join ","
    in
    String.join "|" [ nodesString, edgesString ]


type alias NodeAsId =
    { nodeId : Int }


type alias LinkAsIds =
    { source : Int, target : Int }


layoutParams : Graph a b -> ( String, List NodeAsId, List LinkAsIds )
layoutParams graph =
    let
        nodesAsIds =
            Graph.nodeIds graph
                |> List.map (\id -> { nodeId = id })

        linksAsIds =
            Graph.edges graph
                |> List.map (\{ from, to } -> { source = from, target = to })
    in
    ( graphStructureId graph, nodesAsIds, linksAsIds )



-- MODEL


initialGraph : Graph Threshold ()
initialGraph =
    let
        nodes =
            [ Graph.Node 0 0.5
            , Graph.Node 1 0.2
            , Graph.Node 2 0.8
            , Graph.Node 3 0.7
            ]

        e from to =
            Graph.Edge from to ()

        edges =
            [ e 0 1
            , e 0 2
            , e 2 3
            , e 1 3
            , e 0 3
            ]

        merger from to outgoingLabel incomingLabel =
            outgoingLabel
    in
    Graph.fromNodesAndEdges nodes edges
        |> Graph.symmetricClosure merger


generateGraph generator =
    let
        netGen =
            Tuple.first generator

        ( tSeed, gThresholds ) =
            case Tuple.second generator of
                Dirac loc ->
                    ( Random.initialSeed 0
                    , Random.constant loc
                        |> Random.list netGen.n
                    )

                Normal s mu sigma ->
                    ( Random.initialSeed s
                    , normalGenerator netGen.n mu sigma
                        |> Random.map (List.map (clamp 0 1))
                    )

        ( thresholds, _ ) =
            Random.step gThresholds tSeed

        merger from to outgoingLabel incomingLabel =
            outgoingLabel

        edges =
            Random.step (edgesGenerator netGen.n netGen.p)
                (Random.initialSeed netGen.seed)
                |> Tuple.first
    in
    Graph.fromNodeLabelsAndEdgePairs thresholds edges
        |> Graph.symmetricClosure merger


edgesGenerator n p =
    if p <= 0 then
        Random.constant []

    else if p >= 1 then
        uniquePairs (List.range 0 (n - 1))
            |> Random.constant

    else
        let
            nPossibleEdges =
                (n * (n - 1)) // 2

            possibleEdges =
                uniquePairs (List.range 0 (n - 1))
        in
        Random.list nPossibleEdges (Random.float 0 1)
            |> Random.andThen
                (\fs ->
                    zip fs possibleEdges
                        |> List.filter (\( f, e ) -> f < p)
                        |> List.map Tuple.second
                        |> Random.constant
                )


normalGenerator n mu sigma =
    let
        nDoubles =
            n // 2

        single =
            if modBy 2 n == 0 then
                Random.constant []

            else
                normalGeneratorHelp
                    |> Random.map (\p -> [ Tuple.first p ])
    in
    Random.pair
        single
        (Random.list nDoubles normalGeneratorHelp)
        |> Random.map
            (\( single_, doubles_ ) ->
                doubles_
                    |> List.map (\( s1, s2 ) -> [ s1, s2 ])
                    |> List.concat
                    |> List.append single_
                    |> List.map (\s -> mu + s * sigma)
            )


normalGeneratorHelp =
    Random.pair (Random.float 0 1) (Random.float 0 1)
        |> Random.map
            (\( u, v ) ->
                let
                    factor =
                        sqrt <| -2.0 * logBase 2 u
                in
                ( factor * cos (2 * pi * v)
                , factor * sin (2 * pi * v)
                )
            )


initialHistorySeedInt =
    0


initialStory : Story
initialStory =
    { graph = initialGraph
    , history =
        History
            initialHistorySeedInt
            (Random.initialSeed initialHistorySeedInt)
            (Selection.fromList [])
    }


initialModel : Model
initialModel =
    let
        makeStory : ( Generator, List Transition ) -> ( Generator, Story )
        makeStory ( gen, seq ) =
            ( gen
            , { graph = generateGraph gen
              , history =
                    History
                        initialHistorySeedInt
                        (Random.initialSeed initialHistorySeedInt)
                        (Selection.fromList seq)
              }
            )
    in
    { generator = ( initialNetworkGenerator, initialDirac )
    , story = initialStory
    , playing = False
    , layouts = Dict.fromList []
    , savedStories =
        initialSavedGens
            |> List.map makeStory
            |> Selection.fromList
    }


initialSavedGens =
    [ storySmallMediumSusceptGenerators
    , storySmallPartialCascadesGenerators
    , storySmallSpontaneousGenerators
    , storyLargeSpontaneousGenerators
    , storyLargeStableGenerators
    , storyMediumFewSeedersGenerators
    , storyMediumRobustGenerators
    ]


storySmallMediumSusceptGenerators =
    ( ( { n = 25, p = 0.12, seed = 0 }
      , Dirac 0.3
      )
    , [ Seed 0, Seed 12, Seed 7 ]
    )


storySmallPartialCascadesGenerators =
    ( ( { n = 25, p = 0.12, seed = 0 }
      , Normal 0 0.6 0.15
      )
    , [ Seed 1, Seed 9, Seed 19 ]
    )


storySmallSpontaneousGenerators =
    ( ( { n = 25, p = 0.12, seed = 0 }
      , Normal 0 0.3 0.15
      )
    , []
    )


storyLargeSpontaneousGenerators =
    ( ( { n = 503, p = 0.006, seed = 0 }
      , Normal 0 0.4 0.5
      )
    , []
    )


storyLargeStableGenerators =
    ( ( { n = 503, p = 0.006, seed = 0 }
      , Dirac 0.4
      )
    , []
    )


storyMediumFewSeedersGenerators =
    ( ( { n = 101, p = 0.028, seed = 0 }
      , Dirac 0.3
      )
    , [ Seed 72 ]
    )


storyMediumRobustGenerators =
    ( ( { n = 101, p = 0.081, seed = 0 }
      , Dirac 0.3
      )
    , []
    )


type alias Model =
    { generator : Generator
    , story : Story
    , playing : Bool
    , layouts : Dict String (Computable GraphLayout)
    , savedStories : Selection ( Generator, Story )
    }


type alias Generator =
    ( NetworkGenerator, ThresholdsGenerator )


type alias NetworkGenerator =
    { n : Int
    , p : Float
    , seed : Int
    }


initialNetworkGenerator =
    { n = 25, p = 3 / 25, seed = 0 }


type ThresholdsGeneratorType
    = DiracType
    | NormalType


type ThresholdsGenerator
    = Dirac Float
    | Normal Int Float Float


initialDirac =
    Dirac 0.18


initialNormal =
    Normal 0 0.18 0.05


type Computable a
    = Computing
    | Ready a


type alias GraphLayout =
    Dict Int Position


type alias Story =
    { graph : Graph Threshold ()
    , history : History
    }


touchedNodeIds (History _ _ ts) =
    let
        getId t =
            case t of
                Seed id ->
                    id

                Flip id ->
                    id
    in
    Selection.toList ts
        |> List.map getId
        |> Set.fromList


untouchedNodeIds { graph, history } =
    Set.diff
        (nodeIds graph |> Set.fromList)
        (touchedNodeIds history)


type alias Position =
    { x : Float, y : Float }


type alias Threshold =
    Float


type History
    = History Int Random.Seed (Selection Transition)


transitions (History _ _ t) =
    t


seedInt (History i _ _) =
    i


seed (History _ s _) =
    s


fromGraph graph =
    Story graph (fromSeedInt 0)


fromSeedInt i =
    History i (Random.initialSeed i) (Selection.fromList [])


isEmpty (History _ _ t) =
    Selection.toList t == []


isAtEnd (History _ _ t) =
    aboveS t == []


pushAndSelectWithSeed s t (History i _ ts) =
    History i s (pushAndSelect t ts)


updateTransitions update (History i s t) =
    History i s (update t)


type Transition
    = Flip NodeId
    | Seed NodeId



-- UPDATE


type Msg
    = StepForward
    | StepBackward
    | StepTo Transition
    | Reset
    | SeedNode NodeId
    | HistorySeed String
    | Save
    | Load ( Generator, Story )
    | Play
    | Pause
    | Generate
    | NewGraphLayout ( String, List { nodeId : Int, x : Float, y : Float } )
    | NetworkGeneratorSetN Int
    | NetworkGeneratorSetP Float
    | NetworkGeneratorSetSeedInt String
    | PickThresholdsGenerator ThresholdsGeneratorType
    | ThresholdsGeneratorSetLocation Float
    | ThresholdsGeneratorSetStd Float
    | ThresholdsGeneratorSetSeedInt String
