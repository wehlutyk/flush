module Selection exposing (aboveS, dropAboveSelection, pushAndSelect, sAndBelow, selectNextOrInit, selectPreviousOrNone)

import List.Extra exposing (dropWhile, last, takeWhile)
import List.Selection as Selection


pushAndSelect el selection =
    Selection.toList selection
        |> (::) el
        |> Selection.fromList
        |> Selection.select el


sAndBelow selection =
    selection
        |> Selection.mapSelected
            { selected = \a -> ( True, a )
            , rest = \a -> ( False, a )
            }
        |> Selection.toList
        |> dropWhile (not << Tuple.first)
        |> List.map Tuple.second


aboveS selection =
    selection
        |> Selection.mapSelected
            { selected = \a -> ( True, a )
            , rest = \a -> ( False, a )
            }
        |> Selection.toList
        |> takeWhile (not << Tuple.first)
        |> List.map Tuple.second


selectNextOrInit selection =
    let
        bottom =
            selection
                |> Selection.toList
                |> last
    in
    case ( bottom, last <| aboveS selection ) of
        ( Nothing, _ ) ->
            -- selection is an empty list
            selection

        ( Just b, Nothing ) ->
            -- Top item was selected
            selection

        ( Just b, Just a ) ->
            selection
                |> Selection.select a


selectPreviousOrNone selection =
    case sAndBelow selection of
        [] ->
            Selection.deselect selection

        [ s ] ->
            Selection.deselect selection

        s :: prev :: _ ->
            Selection.select prev selection


dropAboveSelection selection =
    let
        sAndB =
            sAndBelow selection
    in
    case sAndB of
        [] ->
            Selection.fromList []

        head :: tail ->
            sAndB
                |> Selection.fromList
                |> Selection.select head
