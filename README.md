Flush
=====

An interactive demonstration of information cascades on networks, built with Elm.

Set up: `npm install`

Build: `make`

Develop:
```sh
npm run serve &
make watch
```
