ELM_BUILD := elm make src/Main.elm --output=dist/flush.js


dist: clean prep
	$(ELM_BUILD) --optimize


debug: clean prep
	$(ELM_BUILD) --debug


watch:
	@while inotifywait -e close_write $(shell find src -name *.elm) index.js; do echo; $(MAKE) debug; echo; done


watch-dist:
	@while inotifywait -e close_write $(shell find src -name *.elm) index.js; do echo; $(MAKE) dist; echo; done


prep:
	@mkdir -p dist
	@cp node_modules/d3-dispatch/dist/d3-dispatch.min.js dist/
	@cp node_modules/d3-quadtree/dist/d3-quadtree.min.js dist/
	@cp node_modules/d3-timer/dist/d3-timer.min.js dist/
	@cp node_modules/d3-force/dist/d3-force.min.js dist/
	@cp index.js dist/


clean:
	@rm -rf dist/*
